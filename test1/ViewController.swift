//
//  ViewController.swift
//  test1
//
//  Created by Akramul Haque on 26/11/18.
//  Copyright © 2018 Refat. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    var player: AVPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadVideo()
    }
    
    private func loadVideo() {
        
        //this line is important to prevent background music stop
        do {
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryAmbient)
        } catch { }
        
        let path = Bundle.main.path(forResource: "output", ofType:"mp4")
        guard let filePath = path else {
            print("No File is here ")
            return
            
        }
        
        let filePathURL = NSURL.fileURL(withPath: path!)
        let player = AVPlayer(url: filePathURL)
        let playerLayer = AVPlayerLayer(player: player)
        
        playerLayer.frame = self.view.frame
        //playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
        playerLayer.zPosition = -1
        
        self.view.layer.addSublayer(playerLayer)
        
        player.seek(to: kCMTimeZero)
        player.play()
     }
}
